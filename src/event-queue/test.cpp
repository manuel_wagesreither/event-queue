#include <cstdlib>
#include "event.hpp"
#include "queue.hpp"

class test_event_1 : public mnwg::event {
    public:
    test_event_1(std::string name) : mnwg::event(name) {};
};

class test_event_2 : public mnwg::event {
    public:
    test_event_2(std::string name) : mnwg::event(name) {};
};

class test_subscriber_1 : public mnwg::subscriber<test_event_1> {
    public:
    test_subscriber_1() {
        mnwg::queue::get_instance().subscribe<test_event_1>( this );
    }
    ~test_subscriber_1() {
        mnwg::queue::get_instance().unsubscribe<test_event_1>( this );
    }
    virtual void handle( const test_event_1 * event ) {
        std::cout << "Caught event \"" << event->get_name() << "\" in " << this << "." << std::endl;
        _event_1_caught_at_least_twice = _event_1_caught_at_least_once ? true : false;
        _event_1_caught_at_least_once = true;
    }
    bool event_1_caught_exactly_once() {
        return _event_1_caught_at_least_once && !_event_1_caught_at_least_twice;
    }
    private:
    bool _event_1_caught_at_least_once = false;
    bool _event_1_caught_at_least_twice = false;
};

class test_subscriber_2 : public mnwg::subscriber<test_event_2> {
    public:
    test_subscriber_2() {
        mnwg::queue::get_instance().subscribe<test_event_2>( this );
    }
    ~test_subscriber_2() {
        mnwg::queue::get_instance().unsubscribe<test_event_2>( this );
    }
    virtual void handle( const test_event_2 * event ) {
        std::cout << "Caught event \"" << event->get_name() << "\" in " << this << "." << std::endl;
        _event_2_caught_at_least_twice = _event_2_caught_at_least_once ? true : false;
        _event_2_caught_at_least_once = true;
    }
    bool event_2_caught_exactly_once() {
        return _event_2_caught_at_least_once && !_event_2_caught_at_least_twice;
    }
    private:
    bool _event_2_caught_at_least_once = false;
    bool _event_2_caught_at_least_twice = false;
};

class test_subscriber_3
    : public mnwg::subscriber<test_event_1>
    , public mnwg::subscriber<test_event_2> {
    public:
    test_subscriber_3() {
        mnwg::queue::get_instance().subscribe<test_event_1>( this );
        mnwg::queue::get_instance().subscribe<test_event_2>( this );
    }
    ~test_subscriber_3() {
        mnwg::queue::get_instance().unsubscribe<test_event_1>( this );
        mnwg::queue::get_instance().unsubscribe<test_event_2>( this );
    }
    virtual void handle( const test_event_1 * event ) {
        std::cout << "Caught event \"" << event->get_name() << "\" in " << this << "." << std::endl;
        _event_1_caught_at_least_twice = _event_1_caught_at_least_once ? true : false;
        _event_1_caught_at_least_once = true;
    }
    virtual void handle( const test_event_2 * event ) {
        std::cout << "Caught event \"" << event->get_name() << "\" in " << this << "." << std::endl;
        _event_2_caught_at_least_twice = _event_2_caught_at_least_once ? true : false;
        _event_2_caught_at_least_once = true;
    }
    bool event_1_caught_exactly_once() {
        return _event_1_caught_at_least_once && !_event_1_caught_at_least_twice;
    }
    bool event_2_caught_exactly_once() {
        return _event_2_caught_at_least_once && !_event_2_caught_at_least_twice;
    }
    private:
    bool _event_1_caught_at_least_once = false;
    bool _event_1_caught_at_least_twice = false;
    bool _event_2_caught_at_least_once = false;
    bool _event_2_caught_at_least_twice = false;
};


int main( int, char** argv )
{
    test_subscriber_1 my_subscriber_1;
    test_subscriber_2 my_subscriber_2;
    test_subscriber_3 my_subscriber_3;

    mnwg::queue::get_instance().add_event( new test_event_1( "Event 1" ) );
    mnwg::queue::get_instance().add_event( new test_event_2( "Event 2" ) );

    mnwg::queue::get_instance().quit_when_empty();
    mnwg::queue::get_instance().run();

    std::cout << "DEBUG: " << argv[0] << " has shut down gracefully." << std::endl;

    return ( my_subscriber_1.event_1_caught_exactly_once()
        && my_subscriber_2.event_2_caught_exactly_once()
        && my_subscriber_3.event_1_caught_exactly_once()
        && my_subscriber_3.event_2_caught_exactly_once() )
        ? EXIT_SUCCESS : EXIT_FAILURE;
}
