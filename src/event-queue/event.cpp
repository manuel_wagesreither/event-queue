#include "event.hpp"

namespace mnwg {

event::event(std::string name) : _name(name) {}
std::string event::get_name() const { return _name; }

} // namespace mnwg
