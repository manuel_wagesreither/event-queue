#pragma once

#include <string>

namespace mnwg {

class event {
    public:
    event(std::string name);
    virtual ~event() = default;
    std::string get_name() const;

    private:
    const std::string _name;
};

} // namespace mnwg
