#pragma once

#include <mutex>
#include <memory>
#include <queue>
#include <condition_variable>
#include <iostream>
#include <list>
#include <algorithm>
#include "eventfilter.tpp"

namespace mnwg {

class queue {
public:
    queue() {}
    queue(const queue&) = delete;
    void operator=(const queue&) = delete;
    
    static queue& get_instance();

    void add_event( event const * const event );

    template <typename event_type>
    void subscribe(subscriber<event_type>* subscriber);

    template <typename event_type>
    void unsubscribe(subscriber<event_type>* subscriber);

    void run();

    std::queue<event const *>::size_type events_in_queue();
    void quit_when_empty();

private:
    bool wait_for_events();
    void dispatch_event();

    // gets notified at change of both _quit and _events
    std::condition_variable _cv;
    // _m protects synchronizes access to both _quit and _events
    std::mutex _m;
    bool _quit = false;
    std::queue<event const *> _events;

    std::list<std::unique_ptr<const eventfilter_base>> _eventfilters;
};

template <typename event_type>
void queue::subscribe(subscriber<event_type>* subscriber)
{
    const eventfilter<event_type> ef(subscriber);

    // Predicate to check wether eventfilter already in list
    const auto equal_to_new = [&ef](const std::unique_ptr<const eventfilter_base>& eventfilter) {
        return ef == *eventfilter;
    };

    // If not, allow the subscription
    if ( std::none_of(_eventfilters.begin(), _eventfilters.end(), equal_to_new ) ) {
        auto efilter = std::make_unique<const eventfilter<event_type>>( subscriber );
        auto address = efilter.get();
        _eventfilters.emplace_back(std::move(efilter));
        std::cout << "DEBUG: Created evenfilter " << address << ", have " << _eventfilters.size() << " in total." << std::endl;
    }
    else
        std::cout << "WARNING: Eventfilter exists already" << std::endl;
}

template <typename event_type>
void queue::unsubscribe(subscriber<event_type>* subscriber)
{
    const eventfilter<event_type> ef(subscriber);

    // Check if the subscriber really holds a subscription for the event
    const auto it = std::find_if(
        _eventfilters.begin(),
        _eventfilters.end(),
        [&ef](const std::unique_ptr<const eventfilter_base>& eventfilter) {
            return ef == *eventfilter;
        }
    );

    // If not, there is nothing to unsubscribe
    if ( it == _eventfilters.end())
        std::cout << "Unable to unsubscribe, subscription doesn't exist" << std::endl;
    else {
        auto address = it->get();
        _eventfilters.erase(it);
        std::cout << "DEBUG: Removed eventfilter " << address << "; have " << _eventfilters.size() << " remaining." << std::endl;
    }
}

} // namespace mnwg
