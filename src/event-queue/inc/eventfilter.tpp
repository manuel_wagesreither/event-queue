#pragma once

#include <iostream>
#include <memory>
#include "subscriber.tpp"
#include "eventfilter_base.hpp"
#include "event.hpp"

namespace mnwg {

template <typename event_type>
class eventfilter : public eventfilter_base {
    public:
    eventfilter(subscriber<event_type>* subscriber) : _subscriber(subscriber) {}
    virtual bool operator ==(const eventfilter_base &b) const {
        auto* other = dynamic_cast<const eventfilter<event_type>*>(&b);
        if ( !other )
            return false;
        
        if ( other->get_subscriber() != _subscriber )
            return false;
        
        return true;
    }

    virtual ~eventfilter() {}

    virtual void handle( event const * event ) const
    {
        auto casted_event = dynamic_cast< event_type const * >( event );
        if ( casted_event != nullptr ) {
            std::cout << "DEBUG: Eventfilter " << this << ": Handling event " << casted_event << "." << std::endl;
            _subscriber->handle( casted_event );
        } else {
            std::cout << "DEBUG: Eventfilter " << this << ": Ignoring event " << event << "." << std::endl;
        }
    }

    virtual subscriber_base* get_subscriber() const {
        return _subscriber;
    }

    private:
    subscriber<event_type>* _subscriber;
};

} // namespace mnwg
