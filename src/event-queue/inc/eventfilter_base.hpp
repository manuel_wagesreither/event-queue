#pragma once
#include <memory>
#include "event.hpp"
#include "subscriber_base.hpp"

namespace mnwg {
class eventfilter_base {
    public:
    virtual bool operator ==(const eventfilter_base &b) const = 0;
    virtual void handle( event const * event ) const =0;
    virtual subscriber_base* get_subscriber() const =0;
    virtual ~eventfilter_base();
};
}
