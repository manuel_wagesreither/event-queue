#pragma once

#include <memory>
#include "subscriber_base.hpp"

namespace mnwg {

template <typename T>
class subscriber : public subscriber_base {
    public:
    virtual void handle( T const * const event ) =0;
};

} // namespace mnwg
