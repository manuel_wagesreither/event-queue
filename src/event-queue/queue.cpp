#include "queue.hpp"
#include "eventfilter.tpp"
#include "event.hpp"

namespace mnwg {

// Source: https://stackoverflow.com/a/1008289/3787646
queue& queue::get_instance()
{
    static queue instance;
    return instance;
}


void queue::add_event( event const * event )
{
    std::cout << "DEBUG: Adding event "<< event << " to queue." << std::endl;

    std::unique_lock<std::mutex> ul(_m);
    _events.push(event);
    ul.unlock();

    _cv.notify_one();    
}


bool queue::wait_for_events()
{
    std::unique_lock<std::mutex> ul(_m);

    bool continue_handling_events = true;

    _cv.wait(ul,
        [this, &continue_handling_events]
        ()
        {
            if ( !_events.empty() ) {
                std::cout << "DEBUG: Handling event; have " << events_in_queue() -1 << " remaining." << std::endl;
                continue_handling_events = true;
                return true;
            } else if ( _quit ) {
                std::cout << "DEBUG: Event queue empty and stop requested; exiting." << std::endl;
                continue_handling_events = false;
                return true;
            } else {
                // Spurious wakeup, keep waiting.
                std::cout << "DEBUG: Event queue empty and stop not requested, blocking thread." << std::endl;
                return false;
            }
        }
    );
    ul.unlock();

    return continue_handling_events;
}


void queue::dispatch_event()
{
    // Get event from queue
    std::unique_lock<std::mutex> ul(_m);
    auto event = _events.front();
    _events.pop();
    ul.unlock();

    std::cout << "DEBUG: Dispatching event " << event << "." << std::endl;

    // Let every eventfilter check it
    for (auto& eventfilter : _eventfilters)
        eventfilter->handle( event );

    std::cout << "DEBUG: Dispatched event " << event << "." << std::endl;

    delete event;
}


std::queue< event const * >::size_type queue::events_in_queue()
{
    return _events.size();
}


void queue::quit_when_empty() {
    std::cout << "DEBUG: Received stop request." << std::endl;

    std::unique_lock<std::mutex> ul(_m);
    _quit = true;
    ul.unlock();

    _cv.notify_one();
}


void queue::run()
{
    while ( mnwg::queue::get_instance().wait_for_events() )
        mnwg::queue::get_instance().dispatch_event();
}

} // namespace mnwg
