#include <cstdlib>
#include "queue.hpp"
#include "signal-handling.hpp"
#include "supress-echo.hpp"

int main(int, char**)
{
    mnwg::supress_echo();
    mnwg::setup_sighandler();

    mnwg::queue::get_instance().run();

    std::cout << "Shutting down gracefully." << std::endl;

    return EXIT_SUCCESS;
}
