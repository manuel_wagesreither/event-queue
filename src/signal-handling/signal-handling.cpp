#include <csignal>
#include "queue.hpp"


namespace mnwg {

static void sighandler( int signal )
{
    std::cout << "SIGINT received." << std::endl;
    mnwg::queue::get_instance().quit_when_empty();
}

void setup_sighandler()
{
    signal( SIGINT, sighandler );
}

} // namespace mnwg
