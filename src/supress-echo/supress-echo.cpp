#include <stdlib.h>
#include <termios.h>
#include <unistd.h>


namespace mnwg {

static struct termios backup;

static void restore() {
    tcsetattr( STDIN_FILENO, TCSANOW, &backup );
}

// Taken from https://stackoverflow.com/a/42563972.
void supress_echo()
{
    // Backup existing terminal settings
    tcgetattr( STDIN_FILENO, &backup );
    atexit( restore );

    // Change current terminal settings
    struct termios attributes;
    tcgetattr( STDIN_FILENO, &attributes );
    attributes.c_lflag &= ~ ECHO;
    tcsetattr( STDIN_FILENO, TCSAFLUSH, &attributes );
}

} // namespace mnwg
