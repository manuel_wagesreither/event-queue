#include "secure-tcp-server.hpp"
#include "secure-tcp-client.hpp"

#include <vector>
#include <thread>

#define THREAD_COUNT 4


int main(int, char**)
{
    boost::asio::io_context context;

    mnwg::tcp_server server(context, 0);
    mnwg::tcp_client client(context, server.get_port());

    std::vector<std::thread> threads;

    for (int i=0; i < THREAD_COUNT; i++)
        threads.push_back(std::thread([&context]{ context.run(); }));

    for (std::thread& thread : threads)
        thread.join();

    return 0;
}
