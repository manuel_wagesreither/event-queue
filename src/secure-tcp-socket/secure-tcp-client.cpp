#include "secure-tcp-client.hpp"

#include <iostream>
#include <functional>
#include <boost/bind/bind.hpp>

namespace mnwg {

tcp_client::tcp_client(boost::asio::io_context& context, unsigned short port)
    : _socket(context)
{
    const std::string server = "127.0.0.1";
    boost::asio::ip::tcp::endpoint remote_endpoint(boost::asio::ip::address::from_string(server), port);
    _socket.async_connect(remote_endpoint,
                          boost::bind(&tcp_client::connect_cb,
                                      this,
                                      boost::asio::placeholders::error
                          )
    );
    std::cout << "Client: Connecting to " << server << ":" << port << "..." << std::endl;
}


tcp_client::~tcp_client()
{
}


void tcp_client::connect_cb(const boost::system::error_code& error)
{
    if (error) {
        std::cout << error.message() << std::endl;
        return;
    }

    std::cout << "Client: Connected to server." << std::endl;

    _ecdh_handshaker = std::unique_ptr<Ecdh_handshaker>(new Ecdh_handshaker(&_socket, "client", std::bind(&tcp_client::ecdh_done_cb, this)));
}


void tcp_client::ecdh_done_cb()
{
    std::cout << "Client: ECDH completed." << std::endl;
}

} // namespace mnwg
