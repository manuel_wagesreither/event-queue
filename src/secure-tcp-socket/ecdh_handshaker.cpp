#include "ecdh_handshaker.hpp"

#include <iostream>
#include <boost/bind/bind.hpp>

namespace mnwg {

Ecdh_handshaker::Ecdh_handshaker()
{
}


Ecdh_handshaker::Ecdh_handshaker(boost::asio::ip::tcp::socket* socket, const std::string & role, std::function<void()> result_cb)
    : _socket(socket)
    , _strand(socket->get_executor())
    , _role(role)
    , _result_cb(result_cb)
{
    std::cout << "ECDH handshaker (" << _role << "): Programming key transmission..." << std::endl;

    # Usage of strand might be all wrong: https://stackoverflow.com/questions/57015310/boostasiostrand-fails-to-make-async-write-thread-safe#comment100569547_57015802

    boost::asio::async_write( *_socket
                            , boost::asio::buffer(_send_data, 6)
                            , boost::asio::bind_executor( _strand
                                                        , boost::bind( &Ecdh_handshaker::write_cb
                                                                     , this
                                                                     , boost::asio::placeholders::error
                                                                     , boost::asio::placeholders::bytes_transferred
                                                                     )
                                                        )
                            );


    std::cout << "ECDH handshaker (" << _role << "): Programming key reception..." << std::endl;
    boost::asio::async_read(*_socket,
                            boost::asio::buffer(_receive_data),
                            boost::asio::transfer_at_least(4),
                            boost::asio::bind_executor(_strand,
                                                       boost::bind(&Ecdh_handshaker::read_cb,
                                                                   this,
                                                                   boost::asio::placeholders::error,
                                                                   boost::asio::placeholders::bytes_transferred
                                                       )
                            )
    );
}


Ecdh_handshaker& Ecdh_handshaker::operator=(Ecdh_handshaker&& other)
{
    _socket = other._socket;
    return *this;
}


Ecdh_handshaker::~Ecdh_handshaker()
{
}


void Ecdh_handshaker::read_cb(const boost::system::error_code& error, std::size_t length)
{
    if (error) {
        std::cout << error.message() << std::endl;
        return;
    }

    std::cout << "ECDH handshaker (" << _role << "): Message of length " << length << " received." << std::endl;

    _read_cb_called = true;
    if (_write_cb_called)
        _result_cb();
}


void Ecdh_handshaker::write_cb(const boost::system::error_code& error, std::size_t length)
{
    if (error) {
        std::cout << error.message() << std::endl;
        return;
    }

    std::cout << "ECDH handshaker (" << _role << "): Message of length " << length << " sent." << std::endl;

    _write_cb_called = true;
    if (_read_cb_called)
        _result_cb();
}


} // namespace mnwg
