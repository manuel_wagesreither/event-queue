#include <boost/asio.hpp>
#include "ecdh_handshaker.hpp"

namespace mnwg {

class tcp_server {
    public:
    tcp_server(boost::asio::io_context &, unsigned short port);
    ~tcp_server();
    unsigned short get_port();

    void accept_cb(const boost::system::error_code &, boost::asio::ip::tcp::socket);
    void ecdh_done_cb();

    private:
    boost::asio::ip::tcp::acceptor _acceptor;
    boost::asio::ip::tcp::socket _socket;
    std::unique_ptr<Ecdh_handshaker> _ecdh_handshaker;
};

} // namespace mnwg
