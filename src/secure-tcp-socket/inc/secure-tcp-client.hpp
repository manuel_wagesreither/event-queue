#include <boost/asio.hpp>
#include "ecdh_handshaker.hpp"

namespace mnwg {

class tcp_client {
    public:
    tcp_client(boost::asio::io_context &, unsigned short port);
    ~tcp_client();

    void connect_cb(const boost::system::error_code &);
    // void ecdh_done_cb(const boost::system::error_code &, std::vector<unsigned char>);
    void ecdh_done_cb();

    private:
    boost::asio::ip::tcp::socket _socket;
    std::unique_ptr<Ecdh_handshaker> _ecdh_handshaker;
};

} // namespace mnwg
