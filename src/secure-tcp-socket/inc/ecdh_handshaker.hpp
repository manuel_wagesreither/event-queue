#pragma once
#include <boost/asio.hpp>

namespace mnwg {

class Ecdh_handshaker {
    public:
    Ecdh_handshaker();
    Ecdh_handshaker(boost::asio::ip::tcp::socket *, const std::string &, std::function<void()>);
    ~Ecdh_handshaker();

    Ecdh_handshaker& operator=(Ecdh_handshaker &&);

    void read_cb(const boost::system::error_code &, std::size_t);
    void write_cb(const boost::system::error_code &, std::size_t);

    private:
    boost::asio::ip::tcp::socket * _socket = nullptr;
    boost::asio::strand<boost::asio::ip::tcp::socket::executor_type> _strand;
    char _send_data[1024] = "hi there!";
    char _receive_data[1024];
    std::function<void()> _result_cb;
    bool _read_cb_called = false;
    bool _write_cb_called = false;
    std::string _role;
};

} // namespace mnwg
