```plantuml
@startuml
box Server side
Actor business_logic as business_ss
participant secure_tcp_server as server
participant ecdh_handshaker as server_ecdh
end box
box Client side
participant ecdh_handshaker as client_ecdh
participant secure_tcp_client as client
Actor business_logic as business_cs
end box

create server
business_ss -> server: Instantiate
activate server
note over server: Wait for incoming connections

create client
business_cs -> client: Instantiate
activate client

client -> server: Connect

group Elliptic Curve Diffie-Hellman
    par Parallel
        create server_ecdh
        server -> server_ecdh: Instantiate
        activate server_ecdh
    else
        create client_ecdh
        client -> client_ecdh: Instantiate
        activate client_ecdh
    end

    par Parallel
        note over server_ecdh: Generate ed25519 keypair
        server_ecdh -> client_ecdh: pubkey
    else
        note over client_ecdh: Generate ed25519 keypair
        client_ecdh -> server_ecdh: pubkey
    end

    par Parallel
        note over server_ecdh: Derive secret using own privkey and peers pubkey
        server_ecdh -> server: Secret
        destroy server_ecdh
    else
        note over client_ecdh: Derive secret using own privkey and peers pubkey
        client_ecdh -> client: Secret
        destroy client_ecdh
    end
end
@enduml
```
