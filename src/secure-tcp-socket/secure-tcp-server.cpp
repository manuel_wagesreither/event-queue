#include "secure-tcp-server.hpp"

#include <iostream>
#include <boost/bind/bind.hpp>

namespace mnwg {

tcp_server::tcp_server(boost::asio::io_context& context, unsigned short port)
    : _acceptor(context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
    , _socket(context)
{
    _acceptor.async_accept(std::bind(&tcp_server::accept_cb, this, std::placeholders::_1, std::placeholders::_2));
    std::cout << "Server: Running on port " << _acceptor.local_endpoint().port() << ". Awaiting connections..." << std::endl;
}


tcp_server::~tcp_server()
{
    // Add code to cancel acceptor
}

unsigned short tcp_server::get_port()
{
    return _acceptor.local_endpoint().port();
}


void tcp_server::accept_cb(const boost::system::error_code& error, boost::asio::ip::tcp::socket socket)
{
    if (error) {
        std::cout << error.message() << std::endl;
        return;
    }

    std::cout << "Server: Client connected." << std::endl;

    _socket = std::move(socket);
    _ecdh_handshaker = std::unique_ptr<Ecdh_handshaker>(new Ecdh_handshaker(&_socket, "server", std::bind(&tcp_server::ecdh_done_cb, this)));
}


// void ecdh_done_cb(const boost::system::error_code &, std::vector<unsigned char>)
void tcp_server::ecdh_done_cb()
{
    std::cout << "Server: ECDH completed." << std::endl;
}

} // namespace mnwg
